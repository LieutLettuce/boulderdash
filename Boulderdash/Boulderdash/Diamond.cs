﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System;
using System.IO;

namespace Boulderdash
{
    class Diamond : Tile
    {
        // ------------------
        // Data
        // ------------------
        private Level ourLevel;
        private bool isCollide = false;
        private Texture2D nonTouchedTexture;
        private Texture2D touchedTexture;

        private int noOfDiamonds = 0;

        private float timeSinceLastMove = 0;

        private const float MOVE_COOLDOWN = 0.8f;
        // ------------------
        // Behaviour
        // ------------------
        public Diamond(Texture2D newNonTouchedTexture, Texture2D newTouchedTexture ,Level newLevel)
            : base(newNonTouchedTexture)
        {
            ourLevel = newLevel;
            nonTouchedTexture = newNonTouchedTexture;
            touchedTexture = newNonTouchedTexture;

        }
        // ------------------
        public bool TryPush(Vector2 direction)
        {
            // New position the box will be in after the push
            Vector2 newGridPos = GetTilePosition() + direction;

            // Ask the level what is in this slot already
            Tile tileInDirection = ourLevel.GetTileAtPosition(newGridPos);

            // If the target tile is a wall, we can't move there - return false.
            if (tileInDirection != null && tileInDirection is Wall) // Make sure nothing happens when the diamond is in contact with the wall.
            {
                return false;
            }
            if (tileInDirection != null && tileInDirection is Boulder) //Prevent the diamond getting pushed by boulders
            {
                return false;
            }

            if (tileInDirection != null && tileInDirection is Diamond) // Prevent the diamond to get pushed by other diamonds
            {
                return false;
            }

            // Ask the level what is on the floor in this direction
            Tile floorInDirection = ourLevel.GetFloorAtPosition(newGridPos);
     
            // Move our tile (box) to the new position
            return ourLevel.TryMoveTile(this, newGridPos);
        }
        // ------------------
        public bool TryFall()
        {
            // New position the box will be in after the push
            Vector2 newGridPosG = GetTilePosition() + new Vector2(0, +1);
            // Ask the level what is in this slot already
            Tile tileInDirection = ourLevel.GetTileAtPosition(newGridPosG);

            if (tileInDirection == null || tileInDirection is Player)
            {
                return ourLevel.TryMoveTile(this, newGridPosG);
            }
            else
            {
                return false;
            }
        }
        // ------------------
        public override void Update(GameTime gameTime)
        {
            // Add to time since we last moved
            float frameTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            timeSinceLastMove += frameTime;

            // If there is nothing under the boulder let it fall on a timer
            if (timeSinceLastMove >= MOVE_COOLDOWN)
            {
                TryFall();
                timeSinceLastMove = 0;
            }
        }
        // ------------------    
    }
}
